import React, { useState, useEffect } from 'react';
import { NextPage } from 'next';
import tw, { styled, TwStyle } from 'twin.macro';

import Layout from '@/layout/Layout';

export const getServerSideProps = ({ query }: any) => ({ props: query });

const IndexPage: NextPage = ({ country }: any) => {
  const [title, setTitle] = useState('Querying...');

  useEffect(() => {
    const fetchApi = async () => {
      const res = await fetch('api/hello');
      const data = await res.json();
      setTitle(data.title);
    };
    fetchApi();
  }, []);

  return (
    <Layout title="Overanalysed">
      <div tw="text-red-400">hi</div>

      <div tw="text-xl space-y-4 md:space-x-4">
        <Link color="purple" href="https://typescriptlang.org">
          TypeScript
        </Link>
      </div>
    </Layout>
  );
};

const linkStyles: Record<string, TwStyle> = {
  red: tw`text-red-500 hover:text-red-700`,
  yellow: tw`text-yellow-500 hover:text-yellow-700`,
  green: tw`text-green-500 hover:text-green-700`,
  blue: tw`text-blue-500 hover:text-blue-700`,
  indigo: tw`text-indigo-500 hover:text-indigo-700`,
  purple: tw`text-purple-500 hover:text-purple-700`
};

const Link = styled.a(({ color }) => [
  tw`block md:inline font-semibold transition-colors duration-300`,
  color && linkStyles[color]
]);

export default IndexPage;
